import './components/contact.css'
import './App.css'
import Contact from './components/contact'

function Profile() {
  return (
    <div>
      <Contact
        name = { 'Germignon'}
        image = { 'https://img.pokemondb.net/artwork/large/chikorita.jpg'}
        isConnected = { true }
        />
        <Contact
        name = { 'Luxray'}
        image = { 'https://www.pokebip.com/pokedex/images/sugimori/405.png'}
        isConnected = { true }
        />
        <Contact
        name = { 'Roigada'}
        image = {'https://www.pokepedia.fr/images/thumb/5/50/Roigada_de_Conway.png/300px-Roigada_de_Conway.png'}
        isConnected = { false }
        />
    </div>
    
  );
}

export default Profile

