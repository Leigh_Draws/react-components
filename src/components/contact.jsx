export default function Contact ({name, image, isConnected}) {
    return ( 
    <section>
        <div className="Contact_Name">
            <p>{name}</p>
            <div className={isConnected === true ? "true" : "false"}></div>
        </div>
        <img
        className="Contact"
        src={image}
        alt={name}
        width={200}
        height={200}
        style = {{ borderRadius :'50%'}}/>
    </section>
    );

}